/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.common.constant


import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.cache.annotation.EnableCaching
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.PropertySource

import javax.validation.constraints.NotBlank


/**
 * 资源配置文件
 *
 * @author ErYang
 */
@Configuration
@PropertySource("classpath:resource.properties")
@ConfigurationProperties(prefix = "resource")
@EnableCaching
class ResourceConst {

    /**
     * 资源信息集合名称
     */
    @NotBlank
    lateinit var mapName: String

    /**
     * 资源信息集合名称
     */
    @NotBlank
    lateinit var resourceName: String
    /**
     * 资源信息集合
     */
    @NotBlank
    lateinit var resource: String

    /**
     * 公共资源目录名称
     */
    @NotBlank
    lateinit var vendorsName: String
    /**
     * 公共资源目录
     */
    @NotBlank
    lateinit var vendors: String

    /**
     * 主题目录名称
     */
    @NotBlank
    lateinit var defaultName: String
    /**
     * 主题目录
     */
    @NotBlank
    lateinit var default: String

    /**
     * app 目录名称
     */
    @NotBlank
    lateinit var appName: String
    /**
     * app 目录
     */
    @NotBlank
    lateinit var app: String

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ResourceConst class

/* End of file ResourceConst.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/admin/common/constant/ResourceConst.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
