package wang.encoding.mroot.common.util;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * 日期工具类
 *
 * @author ErYang
 */
public class DateUtil {

    /**
     * yyyyMMddHHmmssSSS
     */
    public static final String DATE = "yyyyMMddHHmmssSSS";

    /**
     * yyyyMMdd
     */
    public static final String TIME = "yyyyMMdd";

    /**
     * 采用 ThreadLocal 避免 SimpleDateFormat 非线程安全的问题
     * Key 时间格式
     * Value 解析特定时间格式的 SimpleDateFormat
     */
    private static ThreadLocal<Map<String, SimpleDateFormat>> sThreadLocal = new ThreadLocal<>();

    // -------------------------------------------------------------------------------------------------

    /**
     * 获取解析特定时间格式的 SimpleDateFormat
     *
     * @param pattern 时间格式
     * @return SimpleDateFormat
     */
    private static SimpleDateFormat getDateFormat(final String pattern) {
        Map<String, SimpleDateFormat> strDateFormatMap = sThreadLocal.get();
        if (strDateFormatMap == null) {
            strDateFormatMap = new HashMap<>(16);
        }
        SimpleDateFormat simpleDateFormat = strDateFormatMap.get(pattern);
        if (null == simpleDateFormat) {
            simpleDateFormat = new SimpleDateFormat(pattern, Locale.getDefault());
            strDateFormatMap.put(pattern, simpleDateFormat);
            sThreadLocal.set(strDateFormatMap);
            sThreadLocal.remove();
        }
        return simpleDateFormat;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 时间格式化
     *
     * @param date    要格式化的时间
     * @param pattern 要格式化的类型
     */
    public static String formatDate(final Date date, final String pattern) {
        if (null == date || null == pattern) {
            return null;
        }
        return getDateFormat(pattern).format(date);
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End DateUtils class

/* End of file DateUtils.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/common/util/DateUtils.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
