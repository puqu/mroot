/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.business


import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cache.annotation.EnableCaching
import org.springframework.core.env.Environment
import org.springframework.stereotype.Component
import wang.encoding.mroot.model.enums.ProfileEnum


/**
 * 环境配置
 *
 * @author ErYang
 */
@Component
@EnableCaching
class Profile {

    @Autowired
    private lateinit var environment: Environment

    /**
     * 得到当前环境
     * @return String
     */
    fun getActiveProfile(): String {
        val activeProfiles: Array<String> = environment.activeProfiles
        var active: String = activeProfiles.lastOrNull() ?: ""
        active = when (active) {
            ProfileEnum.DEV.key -> ProfileEnum.DEV.value
            ProfileEnum.TEST.key -> ProfileEnum.TEST.value
            ProfileEnum.PROD.key -> ProfileEnum.PROD.value
            else -> ProfileEnum.DEV.value
        }
        return active
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 检验是否是开发环境
     *
     * @return  Boolean
     */
    fun devProfile(): Boolean {
        // 环境
        val activeProfile: String = this.getActiveProfile()
        return ProfileEnum.DEV.value == activeProfile
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End Profile class

/* End of file Profile.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/admin/common/config/Profile.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
