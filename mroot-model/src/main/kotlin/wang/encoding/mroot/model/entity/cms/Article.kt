/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]
<http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>
<http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.model.entity.cms


import com.baomidou.mybatisplus.activerecord.Model
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableId
import com.baomidou.mybatisplus.annotations.TableName
import com.baomidou.mybatisplus.enums.IdType
import org.hibernate.validator.constraints.Length
import org.hibernate.validator.constraints.Range

import java.math.BigInteger
import java.util.*
import javax.validation.constraints.NotNull
import javax.validation.constraints.Past
import javax.validation.constraints.Pattern
import java.io.Serializable


/**
 * 文章实体类
 *
 * @author ErYang
 */
@TableName("cms_article")
class Article : Model<Article>(), Serializable {

    companion object {

        private const val serialVersionUID = -363681313851103625L

        /* 属性名称常量开始 */

        // -------------------------------------------------------------------------------------------------

        /**
         * 文章ID
         */
        const val ID: String = "id"
        /**
         * 文章ID 别名
         */
        const val ID_ALIAS: String = "a.id"
        /**
         * 标题
         */
        const val TITLE: String = "title"

        // -------------------------------------------------------------------------------------------------

        /* 属性名称常量结束 */

        /**
         * 现有的对象赋值给一个新的对象
         *
         * @param article  Article
         * @return Article
         */
        fun copy2New(article: Article): Article {
            val newArticle = Article()
            newArticle.id = article.id
            newArticle.categoryId = article.categoryId
            newArticle.title = article.title
            newArticle.description = article.description
            newArticle.cover = article.cover
            newArticle.sort = article.sort
            newArticle.view = article.view
            newArticle.level = article.level
            newArticle.linkUri = article.linkUri
            newArticle.show = article.show
            newArticle.status = article.status
            newArticle.gmtCreate = article.gmtCreate
            newArticle.gmtCreateIp = article.gmtCreateIp
            newArticle.gmtModified = article.gmtModified
            return newArticle
        }

        // -------------------------------------------------------------------------------------------------

    }


    /**
     * 文章ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    var id: BigInteger? = null

    // -------------------------------------------------------------------------------------------------


    /**
     * 文章分类ID
     */
    var categoryId: BigInteger? = null

    // -------------------------------------------------------------------------------------------------


    /**
     * 标题
     */
    @Pattern(regexp = "^[a-zA-Z0-9_\\u4e00-\\u9fa5]{2,255}$", message = "validation.title.pattern")
    var title: String? = null


    // -------------------------------------------------------------------------------------------------


    /**
     * 描述
     */
    @NotNull(message = "validation.cms.article.description.length")
    @Length(max = 140, message = "validation.cms.article.description.length")
    var description: String? = null

    // -------------------------------------------------------------------------------------------------


    /**
     * 封面
     */
    @Length(max = 255, message = "validation.cms.article.cover.length")
    var cover: String? = null

    // -------------------------------------------------------------------------------------------------


    /**
     * 排序
     */
    @Range(min = 0, message = "validation.sort.range")
    var sort: Long? = null


    // -------------------------------------------------------------------------------------------------


    /**
     * 浏览量
     */
    @Range(min = 0, message = "validation.cms.article.view.range")
    var view: Long? = null

    // -------------------------------------------------------------------------------------------------


    /**
     * 优先级，数字越大级别越高
     */
    @Range(min = 0, message = "validation.cms.article.level.range")
    var level: Long? = null

    // -------------------------------------------------------------------------------------------------


    /**
     * 外链
     */
    @Length(max = 255, message = "validation.cms.article.linkUri.length")
    var linkUri: String? = null

    // -------------------------------------------------------------------------------------------------


    /**
     * 是否前台可见，1是前后台都可见；2是后台可见
     */
    @NotNull(message = "validation.cms.article.show.range")
    @Range(min = 1, max = 2, message = "validation.cms.article.show.range")
    var show: Int? = null

    // -------------------------------------------------------------------------------------------------


    /**
     * 状态(1是正常，2是禁用，3是删除)
     */
    @NotNull(message = "validation.status.range")
    @Range(min = 1, max = 3, message = "validation.status.range")
    var status: Int? = null


    // -------------------------------------------------------------------------------------------------

    /**
     * 创建时间
     */
    @Past(message = "validation.gmtCreate.past")
    var gmtCreate: Date? = null


    // -------------------------------------------------------------------------------------------------


    /**
     * 创建IP
     */
    @Pattern(regexp = "^[0-9.]{6,50}\$", message = "validation.gmtCreateIp.pattern")
    var gmtCreateIp: String? = null


    // -------------------------------------------------------------------------------------------------


    /**
     * 修改时间
     */
    @Past(message = "validation.gmtModified.past")
    var gmtModified: Date? = null


    // -------------------------------------------------------------------------------------------------

    /**
     * 文章内容
     */
    @TableField(exist = false)
    var articleContent: ArticleContent? = null

    // -------------------------------------------------------------------------------------------------

    override fun pkVal(): BigInteger? {
        return this.id
    }

    // -------------------------------------------------------------------------------------------------

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Article

        if (id != other.id) return false
        if (categoryId != other.categoryId) return false
        if (title != other.title) return false
        if (description != other.description) return false
        if (cover != other.cover) return false
        if (sort != other.sort) return false
        if (view != other.view) return false
        if (level != other.level) return false
        if (linkUri != other.linkUri) return false
        if (show != other.show) return false
        if (status != other.status) return false
        if (gmtCreate != other.gmtCreate) return false
        if (gmtCreateIp != other.gmtCreateIp) return false
        if (gmtModified != other.gmtModified) return false
        if (articleContent != other.articleContent) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id?.hashCode() ?: 0
        result = 31 * result + (categoryId?.hashCode() ?: 0)
        result = 31 * result + (title?.hashCode() ?: 0)
        result = 31 * result + (description?.hashCode() ?: 0)
        result = 31 * result + (cover?.hashCode() ?: 0)
        result = 31 * result + (sort?.hashCode() ?: 0)
        result = 31 * result + (view?.hashCode() ?: 0)
        result = 31 * result + (level?.hashCode() ?: 0)
        result = 31 * result + (linkUri?.hashCode() ?: 0)
        result = 31 * result + (show ?: 0)
        result = 31 * result + (status ?: 0)
        result = 31 * result + (gmtCreate?.hashCode() ?: 0)
        result = 31 * result + (gmtCreateIp?.hashCode() ?: 0)
        result = 31 * result + (gmtModified?.hashCode() ?: 0)
        result = 31 * result + (articleContent?.hashCode() ?: 0)
        return result
    }

    override fun toString(): String {
        return "Article(id=$id, categoryId=$categoryId, title=$title, description=$description, cover=$cover, sort=$sort, view=$view, level=$level, linkUri=$linkUri, show=$show, status=$status, gmtCreate=$gmtCreate, gmtCreateIp=$gmtCreateIp, gmtModified=$gmtModified, articleContent=$articleContent)"
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End Article class

/* End of file Article.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/model/entity/cms/Article.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
