/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.plugin.ueditor;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import wang.encoding.mroot.plugin.ueditor.define.ActionMap;

import java.util.HashMap;
import java.util.Map;


/**
 * 配置管理器
 *
 * @author ErYang
 */
public class ConfigManager {

    /**
     * jsonConfig
     */
    private JSONObject jsonConfig = null;
    /**
     * 涂鸦上传 filename 定义
     */
    private final static String SCRAWL_FILE_NAME = "scrawl";
    /**
     * 远程图片抓取 filename 定义
     */
    private final static String REMOTE_FILE_NAME = "remote";

    /**
     * 通过一个给定的路径构建一个配置管理器
     * 该管理器要求地址路径所在目录下必须存在config.properties文件
     *
     * @param config String
     */
    public ConfigManager(String config) {
        this.initEnv(config);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 验证配置文件加载是否正确
     *
     * @return Boolean
     */
    public Boolean valid() {
        return this.jsonConfig != null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到配置
     *
     * @return JSONObject
     */
    JSONObject getAllConfig() {
        return this.jsonConfig;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 读取配置
     *
     * @param type int
     * @return Map<String                                                                                                                               ,                                                                                                                                                                                                                                                               Object>
     */
    Map<String, Object> getConfig(final Integer type) {
        Map<String, Object> conf = new HashMap<>(16);
        String savePath = null;
        switch (type) {
            case ActionMap.UPLOAD_FILE:
                conf.put("isBase64", "false");
                conf.put("maxSize", this.jsonConfig.getLong("fileMaxSize"));
                conf.put("allowFiles", this.getArray("fileAllowFiles"));
                conf.put("fieldName", this.jsonConfig.getString("fileFieldName"));
                savePath = this.jsonConfig.getString("filePathFormat");
                break;
            case ActionMap.UPLOAD_IMAGE:
                conf.put("isBase64", "false");
                conf.put("maxSize", this.jsonConfig.getLong("imageMaxSize"));
                conf.put("allowFiles", this.getArray("imageAllowFiles"));
                conf.put("fieldName", this.jsonConfig.getString("imageFieldName"));
                savePath = this.jsonConfig.getString("imagePathFormat");
                break;
            case ActionMap.UPLOAD_VIDEO:
                conf.put("maxSize", this.jsonConfig.getLong("videoMaxSize"));
                conf.put("allowFiles", this.getArray("videoAllowFiles"));
                conf.put("fieldName", this.jsonConfig.getString("videoFieldName"));
                savePath = this.jsonConfig.getString("videoPathFormat");
                break;
            case ActionMap.UPLOAD_SCRAWL:
                conf.put("filename", ConfigManager.SCRAWL_FILE_NAME);
                conf.put("maxSize", this.jsonConfig.getLong("scrawlMaxSize"));
                conf.put("fieldName", this.jsonConfig.getString("scrawlFieldName"));
                conf.put("isBase64", "true");
                savePath = this.jsonConfig.getString("scrawlPathFormat");
                break;
            case ActionMap.CATCH_IMAGE:
                conf.put("filename", ConfigManager.REMOTE_FILE_NAME);
                conf.put("filter", this.getArray("catcherLocalDomain"));
                conf.put("maxSize", this.jsonConfig.getLong("catcherMaxSize"));
                conf.put("allowFiles", this.getArray("catcherAllowFiles"));
                conf.put("fieldName", this.jsonConfig.getString("catcherFieldName") + "[]");
                savePath = this.jsonConfig.getString("catcherPathFormat");
                break;
            case ActionMap.LIST_IMAGE:
                conf.put("allowFiles", this.getArray("imageManagerAllowFiles"));
                conf.put("dir", this.jsonConfig.getString("imageManagerListPath"));
                conf.put("count", this.jsonConfig.getInteger("imageManagerListSize"));
                break;
            case ActionMap.LIST_FILE:
                conf.put("allowFiles", this.getArray("fileManagerAllowFiles"));
                conf.put("dir", this.jsonConfig.getString("fileManagerListPath"));
                conf.put("count", this.jsonConfig.getInteger("fileManagerListSize"));
                break;
            default:
                break;

        }
        conf.put("savePath", savePath);
        //conf.put("rootPath", this.rootPath);
        return conf;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 配置
     *
     * @param config String
     */
    private void initEnv(final String config) {
        try {
            this.jsonConfig = JSON.parseObject(config);
        } catch (Exception e) {
            this.jsonConfig = null;
        }

    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到数组
     *
     * @param key String
     * @return String[]
     */
    private String[] getArray(final String key) {
        JSONArray jsonArray = this.jsonConfig.getJSONArray(key);
        String[] result = new String[jsonArray.size()];
        for (int i = 0, len = jsonArray.size(); i < len; i++) {
            result[i] = jsonArray.getString(i);
        }
        return result;
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ConfigManager class

/* End of file ConfigManager.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/plugin/ueditor/ConfigManager.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
