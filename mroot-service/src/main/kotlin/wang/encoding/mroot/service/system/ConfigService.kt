/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.service.system


import com.baomidou.mybatisplus.plugins.Page
import wang.encoding.mroot.common.service.BaseService
import wang.encoding.mroot.model.entity.system.Config
import java.math.BigInteger
import java.util.*

/**
 * 后台 系统配置 Service 接口
 *
 * @author ErYang
 */
interface ConfigService : BaseService<Config> {


    /**
     * 初始化新增 Config 对象
     *
     * @param config Config
     * @return Config
     */
    fun initSaveConfig(config: Config): Config

    // -------------------------------------------------------------------------------------------------

    /**
     * 初始化修改 Config 对象
     *
     * @param config Config
     * @return Config
     */
    fun initEditConfig(config: Config): Config

    // -------------------------------------------------------------------------------------------------

    /**
     * Hibernate Validation 验证
     */
    fun validationConfig(config: Config): String?

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 id 查询 Config  缓存
     *
     * @param id ID
     * @return Config
     */
    fun getById2Cache(id: BigInteger): Config?

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据标识查询 Config
     *
     * @param name 标识
     * @return Config
     */
    fun getByName(name: String): Config?

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据名称查询 Config
     *
     * @param title 名称
     * @return Config
     */
    fun getByTitle(title: String): Config?

    // -------------------------------------------------------------------------------------------------

    /**
     * 新增 系统配置
     *
     * @param config Config
     * @return ID  BigInteger
     */
    fun saveBackId(config: Config): BigInteger?

    // -------------------------------------------------------------------------------------------------

    /**
     * 更新 系统配置
     *
     * @param config Config
     * @return ID  BigInteger
     */
    fun updateBackId(config: Config): BigInteger?

    // -------------------------------------------------------------------------------------------------

    /**
     * 删除 系统配置 (更新状态)
     *
     * @param id BigInteger
     * @return ID  BigInteger
     */
    fun removeBackId(id: BigInteger): BigInteger?

    // -------------------------------------------------------------------------------------------------

    /**
     * 恢复 系统配置 (更新状态)
     *
     * @param id BigInteger
     * @return ID  BigInteger
     */
    fun recoverBackId(id: BigInteger): BigInteger?

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 id 移除 Config 缓存
     *
     * @param id BigInteger
     *
     */
    fun removeCacheById(id: BigInteger)

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据  id 批量移除 Config 缓存
     *
     * @param idArray ArrayList<BigInteger>
     *
     */
    fun removeBatchCacheById(idArray: ArrayList<BigInteger>)

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据条件得到表集合 用于分页 代码生成器专用
     *
     * @param page Page
     * @param tableArray ArrayList<String>
     * @return 集合
     */
    fun listTable2PageByMap(page: Page<Map<String, String>>,
                            tableArray: java.util.ArrayList<String>?): Page<Map<String, String>>?

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据条件得到表集合 代码生成器专用
     *
     * @param map 集合
     * @return 集合
     */
    fun listTableByMap(map: HashMap<String, Any>): List<Map<String, String>>?

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据条件得到表集合 代码生成器专用
     *
     * @param tableName 表名
     * @return 集合
     */
    fun getTableByTableName(tableName: String): List<Map<String, String>>?

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据条件得到表详情 代码生成器专用
     *
     * @param tableName String
     * @return 集合
     */
    fun getTableDetailTableName(tableName: String): List<Map<String, String>>?

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ConfigService interface

/* End of file ConfigService.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/service/system/Config.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
